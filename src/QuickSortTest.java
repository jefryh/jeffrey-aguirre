import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Assert;

public class QuickSortTest {
	QuickSort qs = new QuickSort();
	
	@Test
	public void testQuickSortBestCase() {
		int C[] = {1,2,3,4,5,6,7}; // Mejor Caso
 		qs.quick(C, 0, 6);
		int result[] = {1,2,3,4,5,6,7};
		Assert.assertArrayEquals(result, C);
	}
	@Test
	public void testQuickSortWorstCase() {
		int A[] = {7,6,5,4,3,2,1}; // Peor Caso
 		qs.quick(A, 0, 6);
		int result[] = {1,2,3,4,5,6,7};
		Assert.assertArrayEquals(result, A);
	}
	@Test
	public void testQuickSortMediumCase() {
		int B[] = {4,5,6,7,1,2,3}; // Caso Medio
 		qs.quick(B, 0, 6);
		for(int i=0; i<7; i++){
			System.out.println(B[i]);
		}
		int result[] = {1,2,3,4,5,6,7};
		Assert.assertArrayEquals(result, B);
	}
	
	
	
	public void main(String[] args) {
		
	}

}
