
public class QuickSort {
	public void quick(int A[], int ini, int fin) {
		int pibote=A[ini], i=ini, j=fin, aux;
	 
		while(i<j){
			while(A[i]<=pibote && i<j) 
				i++;
			while(A[j]>pibote) 
				j--;
			if (i<j){                      
				aux= A[i];
				A[i]=A[j];
				A[j]=aux;
			}
		}
		A[ini]=A[j];
		A[j]=pibote;
		if(ini < j-1)
			quick(A,ini,j-1);
		if(j+1 < fin)
			quick(A,j+1,fin);
	}
	
	public void main(String[] args) {
		int A[] = {7,6,5,4,3,2,1};
		quick(A, 0, 6);
		for(int i=0; i<7; i++)
			System.out.println(A[i]);
	}
}
